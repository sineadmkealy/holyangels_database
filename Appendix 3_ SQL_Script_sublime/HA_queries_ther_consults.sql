--to find all patients of GP, id no.....
SELECT first_name, last_name
FROM ha_patients
WHERE gp_id = 'G123'; -- runs ok
--------------------------------------------------------------------------------------------------------------------------
--show all therapist reports for patient x
SELECT *
FROM HA_speech_and_language_reports sl, HA_occuptl_therapy_reports ot, HA_psychology_reports ps, HA_physiotherapy_reports phys
WHERE sl.patient_id = 'P086973' AND ot.patient_id = 'P086973'AND ps.patient_id = 'P086973'AND phys.patient_id = 'P086973';--works
------------------------------------------------------------------------------------------------------------------------------
--27. show all therapist reports for patient x between dates x and y

-- join tables on patient_id
-- Application Express says dates need to be in numeric format, so not usual DD/MMM/YYYY format

-- version 1
SELECT *
FROM HA_speech_and_language_reports sl, HA_occuptl_therapy_reports ot, HA_psychology_reports ps, HA_physiotherapy_reports phys
WHERE sl.patient_id = 'P086973' 
AND sl.patient_id = ot.patient_id 
AND sl.patient_id = ps.patient_id 
AND sl.patient_id = phys.patient_id 
AND sl.rpt_date BETWEEN '05-15-2014' AND '09-18-2015'
AND ot.rpt_date BETWEEN '05-15-2014' AND '09-18-2015'
AND ps.rpt_date BETWEEN '05-15-2014' AND '09-18-2015'
AND phys.rpt_date BETWEEN '05-15-2014' AND '09-18-2015'; 
-- this format runs under SK_personal oracle account (where default setting for date is MMM/DD/YYYY, contrary to WIT profile)

-- version 2
SELECT *
FROM HA_speech_and_language_reports sl, HA_occuptl_therapy_reports ot, HA_psychology_reports ps, HA_physiotherapy_reports phys
WHERE sl.patient_id = 'P086973' 
AND sl.patient_id = ot.patient_id 
AND sl.patient_id = ps.patient_id 
AND sl.patient_id = phys.patient_id 
AND sl.rpt_date BETWEEN '15-05-2014' AND '18-09-2015'
AND ot.rpt_date BETWEEN '15-05-2014' AND '18-09-2015'
AND ps.rpt_date BETWEEN '15-05-2014' AND '18-09-2015'
AND phys.rpt_date BETWEEN '15-05-2014' AND '18-09-2015';
-- doesn't work on WIT profile, not a valid month EVEN THOUGH THIS IS CORRECT DATE FORMAT

-- version 3
SELECT *
FROM HA_speech_and_language_reports sl, HA_occuptl_therapy_reports ot, HA_psychology_reports ps, HA_physiotherapy_reports phys
WHERE sl.patient_id = 'P086973' 
AND sl.patient_id = ot.patient_id 
AND sl.patient_id = ps.patient_id 
AND sl.patient_id = phys.patient_id 
AND sl.rpt_date >= '15-05-2014' AND sl.rpt_date <= '18-09-2015'
AND ot.rpt_date >= '15-05-2014' AND ot.rpt_date <= '18-09-2015'
AND ps.rpt_date >= '15-05-2014' AND ps.rpt_date <= '18-09-2015'
AND phys.rpt_date >= '15-05-2014' AND phys.rpt_date <= '18-09-2015'; 
-- doesn't work on WIT profile, not a valid month EVEN THOUGH THIS IS CORRECT DATE FORMAT
-----------------------------------------------------------------------------------------------------------------------------------------
--24.	Show all prescription for patient x in current year 
-- show all precriptions for patient in PAST 12 months, issued by GP only (excl. paediatrician)
SELECT * FROM HA_prescriptions
WHERE patient_id = 'T64973' AND date_psn > '01-06-2015' AND gp_reg_id = 'G123'; -- works
-------------------------------------------------------------------------------------------------------------------------
--25.	Show all consultant paediatrician reports for patient x in last 3 months?
SELECT  *
FROM HA_paediatric_reports
WHERE patient_id = 'P086973'
AND rpt_date < add_months(SYSDATE, -3); -- runs successfully
-------------------------------------------------------------------------------------------------------------------------
--26.	Show all patients of GP x on database
SELECT first_name, last_name
FROM ha_patients
WHERE gp_id = 'G123'; -- runs
----------------------------------------------------------------------------------------------------------------------------------------
--28.	Show contact details of therapist x, (knowing some of surname details)?
SELECT name, ther_grade, telephone, email 
FROM HA_therapists 
WHERE name LIKE '%_allon%'; -- works
-----------------------------------------------------------------------------------------------------------------------------------
--29.	 Show referral date for patient x to paediatrician or therapeutic services?
--problem: the only column from HA_Referrals that can be used to match to another table is the phn_id column.
SELECT p.first_name, p.last_name, r.referral_date
FROM HA_Patients p, HA_stay_records sr, HA_stay_approval sa, HA_Referrals r
WHERE p.patient_id = sr.patient_id
AND sr.approval_ref = sa.approval_ref
AND sa.phn_id = r.phn_id
AND p.patient_id = 'P086973'; 
-- runs successfully,but not making linkage--tables too disparate
-- needs additional foreign key from HA_patients
-- requires barred relationship from HA_phn, HA_paediatricians, and HA_patients to form a composite primary key encompassing date of referral




-