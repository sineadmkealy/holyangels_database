------------------------------------------------------------------------------------------------------

ALTER TABLE HA_staff
ADD CONSTRAINT fk_HA_departments
FOREIGN KEY (dpt_id)
REFERENCES HA_departments(dpt_id)
ON DELETE CASCADE;

ALTER TABLE HA_public_health_nurses 
MODIFY fax DEFAULT 'none';

ALTER TABLE HA_staff
DROP COLUMN dpt_name;

ALTER TABLE HA_stay_records
ADD rcd_created_by VARCHAR2(40) DEFAULT 'user_name';

ALTER TABLE HA_staff
ADD CONSTRAINT fk_agency_id
FOREIGN KEY (agy_id)
REFERENCES HA_agencies(agy_id)
ON DELETE CASCADE;

-- don't use in end, requires mgr id to be primary key, inappropriate...
ALTER TABLE HA_staff
ADD CONSTRAINT fk_man_id
FOREIGN KEY (mgr_id)
REFERENCES HA_staff(mgr_id)
ON DELETE SET NULL;

ALTER TABLE HA_patients
MODIFY address VARCHAR2(50);

ALTER TABLE HA_therapists
MODIFY ther_grade VARCHAR2(40);

ALTER TABLE HA_staff
ADD CONSTRAINT min_sal CHECK (salary > 0);

ALTER TABLE HA_staff
ADD CONSTRAINT hr_rate CHECK (salary > 0);

ALTER TABLE HA_staff
ADD CONSTRAINT hr_rate CHECK (salary > 0);

-- used variously when creating tables:
DROP TABLE HA_stay_records;
DROP TABLE HA_speech_and_language_report;
DROP TABLE HA_occupational_therapy_report;
DROP TABLE HA_psychology_report;
DROP TABLE HA_physiotherapy_report;




