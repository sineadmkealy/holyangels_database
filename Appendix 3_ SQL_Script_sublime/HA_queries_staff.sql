--------------------------------------------------------------------------------------------------------------------------
--IS NULL tests for unavailable, unassigned, unknown data eg dates where events not scheduled, clients that do not have email etc :
--IS NOT NULL tests for data

SELECT last_name, shift
FROM HA_staff
WHERE shift IS NOT NULL
AND ; -- returns staff assigned to shifts????
-- doesnt work
---------------------------------------------------------------------------------------------------
-- to query two tables in tandem:

-- 17. Show all staff with a nfq_level qualification greater than 7
SELECT first_name, last_name
FROM HA_staff
WHERE staff_id IN (SELECT staff_id FROM HA_training_records
       WHERE course_id IN (SELECT course_id FROM HA_training_providers WHERE nfq_level > '7')
); -- runs succesfully
-----------------------------------------------------------------------------------------------------------------------------
-- show staff rise for permanent full time employes, hired between... and..., of nurse and carer grade
SELECT last_name||' '||salary*1.05
AS "Employee Raise"
FROM HA_staff 
WHERE hire_date BETWEEN '23-Jan-2000' AND '31-Dec-2010'
AND job_title LIKE '%nurse' 
OR job_title LIKE '%carer'; -- works
---------------------------------------------------------------------------------------------------------------------------------
--18.	Show all shift assignments of staff member x for the next 3 months
SELECT  shift_date
FROM HA_shift_assignments
WHERE staff_id = 'D142385'
AND shift_date > add_months(SYSDATE, 3); -- runs successfully!!! must add data

-- Show all sihist scheduled for staff member between dates x and y
SELECT  shift_date
FROM HA_shift_assignments
WHERE staff_id = 'D142385'
AND shift_date BETWEEN '15-May-2016' AND '30-Aug-2016';-- runs successfully!!! must add data
-----------------------------------------------------------------------------------------------------------------------
--19.	Show all training records for staff by name, and staff id
SELECT *
FROM HA_training_records 
WHERE staff_id = 'D142385';--works
------------------------------------------------------------------------------------------------------------------------------
--22.	Show all staff assigned to work stay record commencing date 
SELECT assigned_carer, relief_carer
AS "staff on duty"
FROM HA_stay_records
WHERE comm_date = '05-Dec-2014'; --runs successfully, must add data
--------------------------------------------------------------------------------------------------------------