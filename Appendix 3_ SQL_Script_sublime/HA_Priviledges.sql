-- Privileges (UNABLE TO RUN THE FOLLOWING COMMANDS)
--Application Express saying insufficent priviledges to run these commands...........................?

SELECT HA_patients, status
FROM USER_TABLES;

SELECT HA_stay_records, status
FROM ALL_TABLES;
---------------------------------------------------------------------------------------------------------------------

-- Need to CREATE USERS before granting them PRIVILEDGES, or assigning them to ROLES

--CREATE USER statement that creates a new internal database user and assigns a password
CREATE USER doyleamy -- she is HA Physiotherapist
  IDENTIFIED BY HAphysio --default password
  DEFAULT TABLESPACE tbs_perm_01
  TEMPORARY TABLESPACE tbs_temp_01 ---the default tablespace would be tbs_perm_01 with a quota of 20MB, 
  QUOTA 20M on tbs_perm_01; --and the temporary tablespace would be tbs_temp_01
  -- doesnt run

  CREATE USER oboyleoran -- theoretical database administrator
  IDENTIFIED BY db_oran --default password
  DEFAULT TABLESPACE tbs_perm_01
  TEMPORARY TABLESPACE tbs_temp_01
  QUOTA 20M on tbs_perm_01; 

-- to create an external databse user
  CREATE USER external_itconsult
  IDENTIFIED EXTERNALLY
  DEFAULT TABLESPACE tbs_perm_01
  QUOTA 5M on tbs_perm_01
  PROFILE external_user_profile;

  -- to creat a global databse user
  CREATE USER global_user1
  IDENTIFIED GLOBALLY AS 'CN=manager, OU=division, O=oracle, C=US'
  DEFAULT TABLESPACE tbs_perm_01
  QUOTA 10M on tbs_perm_01;

  -------------------------------------------------------------------------------------------------------------------------------
--GRANT PRIVILEDGES:
--USERS may be granted various privileges to tables. 
--These privileges can be any combination of SELECT, INSERT, UPDATE, DELETE, REFERENCES, ALTER, INDEX, or ALL.

GRANT SELECT ON HA_patients TO rgallagher; -- He is a doctor, read-only
GRANT ALL ON HA_patients TO mdunne; -- She is a health administrator, full access - select, insert, update, delete

REVOKE DELETE on HA_patients FROM rgallagher;
REVOKE ALL ON HA_patients FROM public; --If had granted ALL privileges to public (all users) on the HA_patients table and to revoke them.

--------------------------------------------------------------------------------------------------------------------------------------
--CREATE ROLES AND GRANT TABLE PRIVILEDGES TO THAT ROLE
CREATE ROLE role_therapists; --
GRANT all ON HA_patients TO role_therapists;

CREATE ROLE role_staff;
GRANT select, insert, update, delete ON HA_stay_records TO role_staff;
REVOKE delete ON HA_stay_records FROM role_staff;

CREATE ROLE role_phn --public health nurse
IDENTIFIED BY passwordphn; --password-protected
GRANT all ON HA_patients TO role_phn;
REVOKE delete ON HA-patients FROM role_phn;
---------------------------------------------------------------------------------------------------------------------
-- FUNCTION/PROCEDURAL PRIVILEDGES TO ROLE
-- Eg. Might be used to calculate HSE or Insurance Company invoice for patient stay, based on stay record....
GRANT execute on find_Value to role_staff;
REVOKE execute on find_Value to role_staff;