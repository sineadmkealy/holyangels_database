
-- to investigate indexes:
SELECT *
FROM user_indexes;
--------------------------------------------------------------------------------------------------------
-- indexes for refequently-used queries
-- NB ensure index columns do not evaluate to a null value

CREATE INDEX patients_name_idx ON HA_patients (first_name, last_name);

-- This will use the patients_name_idx to process query more quickly
-- to find patient x record from HA_patients
SELECT * FROM HA_patients
WHERE first_name = 'Joanna'
AND last_name = 'Murphy';
--------------------------------------------------------------------------------------------------------

CREATE INDEX therapist_idx
ON HA_therapists (name, dpt_name);

-- to find the (Occupational) Therapist, by Dept name
SELECT * FROM HA_therapists
WHERE name = 'Byrne, Breda'
AND dept_name = 'Occupational Therapy';

  --------------------------------------------------------------------------------------------
-- to find informeation about a particular stay for patient x
CREATE INDEX stay_idx
  ON HA_stay_records (patient_id, comm_date);

 DROP INDEX stay_idx;

SELECT * FROM HA_stay_records
WHERE patient_id = 'R546973'
AND comm_date = 'Aug-14-2015';

-------------------------------------------------------------------------------------------------
-- to locate particular therapy programmes for patient, dated x

CREATE INDEX ot_reports_idx
ON HA_occuptl_therapy_reports (rpt_date, patient_id);


SELECT * FROM HA_occuptl_therapy_reports
WHERE patient_id = 'R546973'
AND rpt_date = 'Aug-14-2014';

CREATE INDEX sl_reports_idx
ON HA_speech_and_language_reports (rpt_date, patient_id);
 
CREATE INDEX psych_reports_idx
ON HA_psychology_reports (rpt_date, patient_id);

CREATE INDEX phys_reports_idx
ON HA_physiotherapy_reports (rpt_date, patient_id);

-------------------------------------------------------------------------------------------------------------------
-- to check what shifts particular staff assigned for
CREATE INDEX shiftassign_idx
ON HA_shift_assignments (shift_date, staff_id);
-------------------------------------------------------------------------------------------------------------------
-- to check that isurance company will cover the proposed stay, HA nurse may call to check
CREATE INDEX health_ins_idx
ON HA_health_insurances (comp_name, tel_no);
-------------------------------------------------------------------------------------------------------------------
 --to check contact number for gp
 CREATE INDEX gp_tel_idx
 ON HA_general_practitioners (name , telephone);
 ------------------------------------------------------------------------------------------------------------------
-- to check phn telephone no
CREATE INDEX phn_tel_idx
 ON HA_public_health_nurses (name , telephone);
 ----------------------------------------------------------------------------------------------------------------