
--16.  to find patients using a walking frame--
--Queries on inner join tables---
SELECT first_name, last_name
FROM ha_patients
WHERE patient_id IN (SELECT patient_id FROM ha_mobility_needs WHERE walkg_frame = 'True'); -- runs successfully


--OR more complex innerjoin terminolgy - too complicated

--16. to find all patients of a particular GP that use a walking frame
--use of aliases
SELECT p.first_name, p.last_name, gp.name 
FROM ha_patients p
LEFT OUTER JOIN ha_general_practitioners gp
ON p.gp_id = gp.gp_id
WHERE patient_id IN (SELECT patient_id FROM ha_mobility_needs WHERE walkg_frame = 'True');

--16.  to find patients with complex needs, communication difficulties --
SELECT first_name, last_name
FROM ha_patients
WHERE patient_id IN (SELECT patient_id FROM HA_complex_needs WHERE communication = 'True'); -- runs successfully
------------------------------------------------------------------------------------------------------------------------------
--5. show pertinent details from all stay records for patient x
SELECT patient_id, comm_date, assigned_carer
FROM HA_stay_records
WHERE patient_id = 'd4I9373'
ORDER BY comm_date DESC;
-- to reverse the order of records
-- runs successfully

--1. show all stay records for patient x in current year
SELECT patient_id, comm_date, assigned_carer
FROM HA_stay_records
WHERE patient_id = 'd4I9373'
AND comm_date > '01-Jan-2016'
ORDER BY comm_date DESC; -- runs successfully
-- to reverse the order of records

-- to see what activities undertaken on recent stays by patient, in order to schedule different ones for forthcoming stay
SELECT day_act
FROM HA_stay_records
WHERE patient_id = 'd4I9373';
ORDER BY comm_date DESC; -- runs successfully
-- to reverse the order of records
------------------------------------------------------------------------------------------------------------------------------------
-- 4. Show all patients and their activities for the specified (stay record) date
SELECT p.first_name, p.last_name, sr.day_act -- use aliases to condense query
FROM HA_patients p, HA_stay_records sr
WHERE p.patient_id = sr.patient_id
AND sr.comm_date = '05-Dec-2015'; --runs successfully, shows 2 patients

--4. Show all patients present on stay record date...
SELECT first_name, last_name
FROM HA_patients
WHERE patient_id IN (SELECT patient_id FROM HA_stay_records
		WHERE comm_date = '05-Dec-2015'); -- similar query, different syntax
--runs succesfully, shows 2 patients
----------------------------------------------------------------------------------------------------------------------------------------
-- 3. Show assigned carer, medication consent by and possible prescriptions administered for patient x on stay_date x?
SELECT sr.assigned_carer, sr.med_consent_from, pr.date_psn
FROM HA_stay_records sr, HA_prescriptions pr
WHERE sr.patient_id = 'M6597473'AND pr. patient_id = 'M6597473'
AND sr.comm_date > pr.date_psn
AND sr.comm_date = '01-09-2015'; -- works
----------------------------------------------------------------------------------------------------------------------
-- to check staff assigned to future shifts where staff assigned and patient_id empty ie stay availability
-- uses barred relationship steps
SELECT first_name, last_name
FROM HA_staff
WHERE staff_id IN (SELECT staff_id FROM HA_shift_assignments
       WHERE shift_id IN (SELECT shift_id from HA_shifts));--works
-----------------------------------------------------------------------------------------------------------------------------
--12. Check for any forthcoming stay vacancies where phn approval given, requested carer on duty and no patients assigned for stay
SELECT first_name, last_name
FROM HA_staff
WHERE staff_id IN (SELECT staff_id FROM HA_shift_assignments
       WHERE shift_id IN (SELECT shift_id from HA_shifts))
AND staff_id = 'D142385'
;--works

-- test where staff scheduled but no patient yet assigned, ie. stay availability
SELECT comm_date
FROM HA_stay_records
WHERE approval_ref is NOT NULL 
AND assigned_carer is NOT NULL
AND patient_id IS NULL
AND comm_date > SYSDATE
; --runs ok 
---------------------------------------------------------------------------------------------------------------------------------------
--12.  show phn approvals for forthcoming proposed stay records of patient x
SELECT approval_ref, approval_date 
FROM HA_stay_approval 
WHERE approval_ref IN (SELECT approval_ref FROM HA_stay_records
WHERE comm_date > SYSDATE
AND patient_id = 'd4I9373'); --works, returns one date
 ---------------------------------------------------------------------------------------------------------------------------------------
 --	2. Show all details for patient x from HA_patients profile
 SELECT * 
 FROM HA_patients
 WHERE patient_id = 'P086973';-- works ok
 -------------------------------------------------------------------------------------------------------------------------------------
 -- show if patient x hold vhealth insurance
SELECT first_name, last_name, ins_mem_no
FROM HA_patients
WHERE patient_id = 'R546973'; -- works, shows HI member no
--------------------------------------------------------------------------------------------------

