INSERT INTO HA_health_insurances VALUES ('VHI', 'VHI', 015551234, 015551234, 'info@vhi.ie');
INSERT INTO HA_health_insurances VALUES ('LIBERTY', 'LIBERTY', 015551234, 015551234, 'info@liberty.ie');
INSERT INTO HA_health_insurances VALUES ('GLO', 'GLOHEALTH', 015551234, 015551234, 'info@glo.ie');
---------------------------------------------------------------------------------------------------------------------------------------------------
INSERT INTO HA_general_practitioners 
VALUES ('G123', 'Gallagher, Ronan', 'Bennekerry, Carlow', '0595551234', '0875551234', '0595551234', 'gallagher@gmail.com');
INSERT INTO HA_general_practitioners 
VALUES ('G456', 'Byrne, Mary', 'Tullow Street, Carlow', '0595551234', '0875551234', '0595551234', 'byrne@gmail.com');
INSERT INTO HA_general_practitioners 
VALUES ('G789', 'Lawlor, John', 'Rutland, Carlow', '0595551234', '0875551234', '0595551234', 'lawlor@gmail.com');
--------------------------------------------------------------------------------------------------------------------------------------------
INSERT INTO HA_departments
VALUES ('ha_day01', 'HA Daycare');
INSERT INTO HA_departments
VALUES ('ha_resp02', 'HA Respite');
--------------------------------------------------------------------------------------------------------------------------------------------
INSERT INTO HA_paediatricians
VALUES ('P010', 'Quinn, John', 'Mary', 'KIDS', 'St Lukes, Kilkenny', '0595551234', '0875551234', '0595551234', 'quinn@stlukes.ie');
INSERT INTO HA_paediatricians
VALUES ('P020', 'Waleron, David', 'Susan', 'KIDS', 'St Lukes, Kilkenny', '0595551234', '0875551234', '0595551234', 'waleron@stlukes.ie');
--------------------------------------------------------------------------------------------------------------------------------------------
INSERT INTO HA_public_health_nurses
VALUES ('N010', 'Dunne, Mary', 'PH', 'Shamrock Plaza, Carlow', '0595551234', '0875551234', '0595551234', 'dunne@carlowhealth.ie');
INSERT INTO HA_public_health_nurses
VALUES ('N020', 'Murphy, Brigid', 'PH', 'Shamrock Plaza, Carlow', '0595551234', '0875551234', '0595551234', 'murphy@carlowhealth.ie');
--------------------------------------------------------------------------------------------------------------------------------------------
INSERT INTO HA_referrals
VALUES ('PHR065', '13-Jan-2014', 'N020', 'P020');
INSERT INTO HA_referrals
VALUES ('P086973', '13-Jun-2014', 'N020', 'P020');
INSERT INTO HA_referrals
VALUES ('P087873', '23-Apr-2015', 'N020', 'P020');
INSERT INTO HA_referrals
VALUES ('P086052', '19-Aug-2013', 'N020', 'P020');
--------------------------------------------------------------------------------------------------------------------------------------------
INSERT INTO HA_patients
VALUES ( 'd4I9373', 'John', 'Byrne', '13-Mar-2008', '8475749P', '5 the Willows, Carlow', '', 'Gen Learn Disabil', '09-Oct-2011', 'Anne Phelan', '0874652431', '', '', '', 'N010', 'G456', 'VHI');
INSERT INTO HA_patients
VALUES ( 'd4I97473', 'Joanna', 'Murphy', '07-May-2012', '713149P', '10 the Burrows, Carlow', '', 'Mild GLND', '09-Oct-2014', 'Amy Ryan', '0853422431', '', '', '76254', 'N020', 'G789', 'GLO');
INSERT INTO HA_patients
VALUES ( 'M6597473', 'Brid', 'Byrne', '09-May-2010', '713135K', 'Russellstown, Carlow', '', 'Severe GLND', '09-Dec-2015', 'Grace Byrne', '0876231431', 'John Byrne', '0732514352', '8132456', 'N010', 'G123', 'LIBERTY');
INSERT INTO HA_patients
VALUES ( 'P086973', 'Rose', 'Lyons', '24-June-2009', '716476H', '7 The Oaks, Tullow', 'hannahobrien@gmail.com', 'Global Delay', '07-April-2014', 'Hannah OBrien', '082865431', 'Jack Cleary', '083764352', '8132321', 'N020', 'G789', 'VHI');
INSERT INTO HA_patients
VALUES ( 'R546973', 'Harry', 'Murphy', '13-Jul-2006', '671645T', '7 The Towers, Tullow', 'joannkealy@gmail.com', 'Global Delay', '07-April-2010', 'Joanne Kealy', '086985431', 'Pat Doyle', '08386552', '8131221', 'N020', 'G789', 'VHI');
INSERT INTO HA_patients
VALUES ( 'T64973', 'Hannah', 'Murphy', '13-Feb-2005', '671634E', '7 Rutland, Tullow', '', 'DownsSyndrome', '07-April-2007', 'Joanne Murphy', '0835685431', 'Pat Murphy', '083863512', '71243221', 'N010', 'G123', 'LIBERTY');

INSERT INTO HA_mobility_needs VALUES ('d4I97473', '', 'True', '');
INSERT INTO HA_feeding_needs VALUES ('M6597473', 'True', '', '', 'True');
INSERT INTO HA_complex_needs VALUES ('T64973', 'True', 'True', '', '', '', 'True');

-- to verify the data in the table:
SELECT * FROM HA_patients;
SELECT * FROM HA_complex_needs;
 --------------------------------------------------------------------------------------------------------------------------------------------
INSERT INTO HA_staff VALUES ('RB9837', 'Care Manager', 'Helen', 'Moore', '10-Dec-1965', 'New Oak Estate, Carlow', 
	0593254864, '', '15-Feb-2001', 'T6543R', 'PFTE', 50000, NULL, 'ha_resp02', '', ''); -- works finally!!!!. 
--NOTE no manager ID (foreign key), as she is manager herself
--the added condition where salary, hourly rate >0, means hourly rate must be entered as NULL                !!!!!!!!!!!!!!

INSERT INTO HA_staff VALUES ('HA4786', 'Assistant Carer', 'Mary', 'Doyle', '13-Mar-1974', 'Hillview, Carlow', 
	058735452, '', '28-July-2010', 'P1267N', 'PFTE', 30000, NULL, 'ha_resp02', '', 'Moore, Helen'); -- PERM FULLTIME EMPLOYEE
INSERT INTO HA_staff VALUES ('D142385', 'Carer Assistant', 'Mary', 'OBrien', '3-Jan-1967', '5 Businfield Crescent, Tullow, Carlow', 
	0599154321, '', '3-Jan-1967', '768422P', 'PPTE', NULL, 15, 'ha_resp02', '', 'Moore, Helen'); -- PERM TEMPORARY EMPLOYEE
INSERT INTO HA_staff VALUES ('D142386', 'Nurse', 'Joan', 'OToole', '3-April-1955', '5 Brownshill, Tullow, Carlow', 
	0599155671, '', '3-Jan-1984', '768422P', 'TPTE', NULL, 30, 'ha_resp02', 'Nurseworks', 'Moore, Helen'); -- TEMPORARY AGENCY WORKER
INSERT INTO HA_staff VALUES ('D142385', 'Carer Assistant', 'Mary', 'OBrien', '3-Jan-1967', '5 Businfield Crescent, Tullow, Carlow', 
	0599154321, '', '3-Jan-1967', '768422P', 'PPTE', NULL, 15, 'ha_resp02', '', 'Moore, Helen'); -- PERM TEMPORARY EMPLOYEE
INSERT INTO HA_staff VALUES ('D142421', 'Carer Assistant', 'Jean', 'OBrien', '27-April-1980', '5 the Meadow, Ballon, Co. Carlow', 
	0599154214, '', '3-Jan-2014', '768422I', 'PPTE', NULL, 15, 'ha_day01', '', 'Moore, Helen'); -- PERM TEMPORARY EMPLOYEE
 --------------------------------------------------------------------------------------------------------------------------------------------
INSERT INTO HA_stay_approval VALUES ('PHN_00452', '25-May-2014', 'N020');
INSERT INTO HA_stay_approval VALUES ('PHN_00453', '27-May-2015', 'N010');
INSERT INTO HA_stay_approval VALUES ('PHN_00454', '25-Mar-2016', 'N020');
INSERT INTO HA_stay_approval VALUES ('PHN_00455', '5-Jan-2016', 'N020');
INSERT INTO HA_stay_approval VALUES ('PHN_00456', '5-Oct-2015', 'N010');
 --------------------------------------------------------------------------------------------------------------------------------------------
INSERT INTO HA_prescriptions VALUES ('13-Jan-2016', 'T64973', 'GP', 'G123','P010'); --runs
DELETE FROM HA_prescriptions WHERE date_psn = '13-Jan-2016' AND patient_id = 'T64973';

INSERT INTO HA_prescriptions VALUES ('13-Mar-2015', 'T64973', 'GP', 'G123','P010'); --runs
INSERT INTO HA_prescriptions VALUES ('13-Apr-2015', 'T64973', 'GP', 'G123','P010'); --runs same patient id (composite key check working)
INSERT INTO HA_prescriptions VALUES ('13-Jun-2015', 'P086973', 'GP', 'G123','P010'); --runs
INSERT INTO HA_prescriptions VALUES ('13-Aug-2015', 'P086973', 'GP', 'G123','P010'); --runs same patient id
INSERT INTO HA_prescriptions VALUES ('13-Oct-2015', 'M6597473', 'GP', 'G123','P010'); --runs
INSERT INTO HA_prescriptions VALUES ('13-Dec-2015', 'M6597473', 'GP', 'G123','P010'); --runs same patient id
 --------------------------------------------------------------------------------------------------------------------------------------------
INSERT INTO HA_paediatric_reports VALUES ('13-Jun-2015', 'P010', 'P086973'); --runs
INSERT INTO HA_paediatric_reports VALUES ('13-Aug-2015', 'P010','P086973'); --runs same patient id
 --------------------------------------------------------------------------------------------------------------------------------------------
 --Stay Records
INSERT INTO HA_stay_records 
VALUES ('d4I9373', 'PHN_00452', 'ha_resp02', '05-June-2014', 3, 2, 'D142386', 'D142385', 'Anne Phelan', 'CIN', 'Odeon', '', '');
INSERT INTO HA_stay_records
VALUES ('d4I9373', 'PHN_00453', 'ha_resp02', '05-Dec-2015', 2, 2, 'D142385', 'D142385', 'Anne Phelan', 'GDN', '', 'Delta Gardens', '');
INSERT INTO HA_stay_records
VALUES ('P086973', 'PHN_00453', 'ha_resp02', '05-Dec-2015', 2, 2, 'D142385', 'D142385', 'Hannah OBrien', 'GDN', '', 'Delta Gardens', '');
DELETE FROM HA_stay_records
WHERE patient_id = 'P086973' AND approval_ref = 'PHN_00453';
INSERT INTO HA_stay_records
VALUES ('P086973', 'PHN_00456', 'ha_resp02', '05-Dec-2015', 2, 2, 'D142385', 'D142385', 'Hannah OBrien', 'GDN', '', 'Delta Gardens', '');
INSERT INTO HA_stay_records
VALUES ('d4I9373', 'PHN_00454', 'ha_resp02', '05-June-2016', 3, 2, 'D142386', 'D142385', 'Anne Phelan', 'PL', '', '', 'Graigue Pool');
INSERT INTO HA_stay_records
VALUES ('d4I9373', 'PHN_00455', 'ha_resp02', '15-Jan-2016', 3, 2, 'D142386', 'D142385', 'Anne Phelan', 'CIN', 'Odeon', '', '');
 --------------------------------------------------------------------------------------------------------------------------------------------
--Occupational Therapists
INSERT INTO HA_therapists
VALUES ('OTI9863', 'Byrne, Breda', 'Occupational Therapy', 'Bethany House, Carlow', 'Senior Occupational Therapist', '0818 3498567', '', '', 'bredabyrned@hse.ie');
INSERT INTO HA_therapists
VALUES ('OTI9842', 'Ryan, Mary', 'Occupational Therapy', 'Bethany House, Carlow', 'Assistant Occupational Therapist', '0818 3498567', '', '', 'maryryan@hse.ie');
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--S&L Therapists
INSERT INTO HA_therapists
VALUES ('SLI9845', 'Fallon, Ann', 'Speech and Language', 'Bethany House, Carlow', 'Senior Speech and Language Therapist', '0818 3498549', '', '', 'annfallon@hse.ie');
INSERT INTO HA_therapists
VALUES ('SLI9878', 'Mulhall, Tracey', 'Speech and Language', 'Bethany House, Carlow', 'Assistant Speech and Language Therapist', '0818 34985549', '', '', 'traceymulhall@hse.ie');
 -------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  --Psychologists
INSERT INTO HA_therapists
VALUES ('PSI98242', 'Foley, John', 'Psychology', 'Shamrock Plaza, Carlow', 'Senior Psychologist', '0818 349852438', '', '', 'johnfoley@hse.ie');
INSERT INTO HA_therapists
VALUES ('PSI98217', 'Coughlan, Catriona', 'Psychology', 'Shamrock Plaza, Carlow', 'Assistant Psychologist', '0818 349852438', '', '', 'catrionacoughlan@hse.ie');
 -------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  --Physiotherapists
INSERT INTO HA_therapists
VALUES ('PHI9852', 'Doyle, Amy', 'Physiotherapy', 'Bethany House, Carlow', 'Senior Physiotherapist', '0818 3498567', '', '', 'amydoyle@hse.ie');
INSERT INTO HA_therapists
VALUES ('PHI98011', 'Nolan, Shannon', 'Physiotherapy', 'Bethany House, Carlow', 'Assistant Physiotherapist', '0818 3498567', '', '', 'shannonnolan@hse.ie');
--------------------------------------------------------------------------------------------------------------------------------------------------------------- 
-- OT Reports
INSERT INTO HA_occuptl_therapy_reports
VALUES ('13-Mar-2015', 'OTI9863', 'd4I9373','', 'severe',  'moderate', 'good', '', 'na');
INSERT INTO HA_occuptl_therapy_reports
VALUES ('9-Feb-2016', 'OTI9842', 'P086973', 'OT Solutions Ltd', 'moderate', 'moderate', 'poor','dyxpraxia', 'na');
INSERT INTO HA_occuptl_therapy_reports 
VALUES ('6-Aug-2015', 'OTI9863', 'R546973', 'Hall McNight', 'good',  'moderate', 'moderate', 'dyxpraxia', 'na');
INSERT INTO HA_occuptl_therapy_reports
VALUES ('13-Sep-2014', 'OTI9842', 'T64973', 'OT Solutions Ltd', 'severe',  'moderate', 'good', '', '');
---------------------------------------------------------------------------------------------------------------------------------------------------------------
--S&L Reports
INSERT INTO HA_speech_and_language_reports VALUES ('13-Nov-2015', 'SLI9845', 'd4I9373', 5, 6, 4,5);
INSERT INTO HA_speech_and_language_reports VALUES ('09-Jan-2016', 'SLI9878', 'M6597473', 3, 2, 3,4);
INSERT INTO HA_speech_and_language_reports VALUES ('06-Mar-2013', 'SLI9845', 'R546973', 5, 5, 7,6);
INSERT INTO HA_speech_and_language_reports VALUES ('13-Nov-2015', 'SLI9878', 'T64973', 2, 6, 4,5);
----------------------------------------------------------------------------------------------------------------------
-- Psychology Reports
INSERT INTO HA_psychology_reports 
VALUES ('14-Mar-2014', 'PSI98242', 'M6597473', '', '', 'high_average', '', '', '', '', '', '', '');
INSERT INTO HA_psychology_reports 
VALUES ('09-Feb-2016', 'PSI98217', 'T64973', '',  '', '', '', '', '', '', '', 'True', '');
----------------------------------------------------------------------------------------------------------------
-- Physiotherpy Reports
INSERT INTO HA_physiotherapy_reports
VALUES ('14-Jul-2015', 'PHI9852', 'd4I97473', '', '', 'True', '', '', '', '', '', 'True', '');
INSERT INTO HA_physiotherapy_reports
VALUES ('14-Jul-2013', 'PHI9852', 'M6597473', '', '', 'True', '', '', '', 'True', '', '', '');
INSERT INTO HA_physiotherapy_reports
VALUES ('14-Jul-2011', 'PHI98011', 'R546973', '', 'True', '', '', '', '', '', '', '', 'True');
INSERT INTO HA_physiotherapy_reports
VALUES ('14-Feb-2016', 'PHI98011', 'T64973', '', '', '', '', '', 'True', '', '', '', 'True');
--------------------------------------------------------------------------------------------------------------------