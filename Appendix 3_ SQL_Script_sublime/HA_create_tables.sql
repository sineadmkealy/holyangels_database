CREATE DATABASE holy_angels; --unable to create--

CREATE TABLE HA_health_insurances (
comp_id VARCHAR2(40) NOT NULL,
comp_name VARCHAR2(20) NOT NULL,
tel_no NUMBER NOT NULL,
fax NUMBER(16,0) NOT NULL,
email VARCHAR2(30) NOT NULL,
CONSTRAINT HA_health_ins_com_id_pk PRIMARY KEY(comp_id)
);
CREATE TABLE HA_departments (
dpt_id VARCHAR2(16) CONSTRAINT HA_departments_pk PRIMARY KEY,
name VARCHAR2(30) NOT NULL
);
CREATE TABLE HA_agencies (
agy_id VARCHAR2(16) NOT NULL,
name VARCHAR2(30) NOT NULL,
CONSTRAINT agencies_agy_id_pk PRIMARY KEY(agy_id)
);
---------------------------------------------------------------------------------------------------------------------
CREATE TABLE HA_shifts (
shift_id VARCHAR2(16) NOT NULL,
description VARCHAR2(30) NOT NULL,
CONSTRAINT HA_shifts_shift_id_pk PRIMARY KEY(shift_id)
);
-- created without staff_id fk initially
CREATE TABLE HA_shift_assignments (
shift_date DATE NOT NULL,
shift_id VARCHAR2(16),
staff_id VARCHAR2(8),
CONSTRAINT HA_shift_assignments_pk PRIMARY KEY (shift_id, staff_id),
CONSTRAINT asn_shift_fk FOREIGN KEY (shift_id) REFERENCES HA_shifts(shift_id),
CONSTRAINT asn_staff_fk FOREIGN KEY (staff_id) REFERENCES HA_staff(staff_id)
);
ALTER TABLE HA_shift_assignments
ADD CONSTRAINT fk_staff 
FOREIGN KEY (staff_id)
REFERENCES HA_staff(staff_id)
ON DELETE CASCADE;
-----------------------------------------------------------------------------------------------------------------------
-- ONE TABLE SUBTYPE IMPLEMENTATION --
CREATE TABLE HA_staff (
staff_id VARCHAR2(8) CONSTRAINT staff_pk PRIMARY KEY,
job_title VARCHAR2(30) NOT NULL,
first_name VARCHAR2(12) NOT NULL,
last_name VARCHAR2(15) NOT NULL,
dob DATE NOT NULL,
address VARCHAR2(40),
telephone NUMBER(16) NOT NULL,
email VARCHAR2(25), 
hire_date DATE,
pps_no VARCHAR2(8) NOT NULL,
epe_type VARCHAR2(30) NOT NULL,
salary NUMBER(10), 
hourly_rate NUMBER(6),
dpt_id VARCHAR2(16) NOT NULL,
agy_id VARCHAR2(16), 
mgr_id VARCHAR2(16),
CONSTRAINT pfte_ck CHECK (epe_type = 'PFTE' AND salary is not null AND hourly_rate is null AND agy_id is null), 
CONSTRAINT ppte_ck CHECK (epe_type = 'PPTE' AND hourly_rate is not null AND salary is null AND agy_id is null), 
CONSTRAINT tpte_ck  CHECK (epe_type = 'TPTE' AND hourly_rate is not null AND salary is null AND agy_id is not null) 
);
-- PROBLEM WITH CONFLICTING CHECK CONSTRAINT,  REVISED SCRIPT:
ALTER TABLE HA_Staff DROP CONSTRAINT pfte_ck;
ALTER TABLE HA_Staff DROP CONSTRAINT ppte_ck;
ALTER TABLE HA_Staff DROP CONSTRAINT tpte_ck;

ALTER TABLE HA_Staff ADD CONSTRAINT epe_ck CHECK (
(epe_type = 'PFTE' AND hourly_rate is null AND salary is not null AND agy_id is null) 
OR (epe_type = 'PPTE' AND hourly_rate is not null AND salary is null AND agy_id is null) 
OR (epe_type = 'TPTE' AND hourly_rate is not null AND salary is null AND agy_id is not null)
);
------------------------------------------------------------------------------------------------------------------------
CREATE TABLE HA_training_providers (
course_id VARCHAR2(16) CONSTRAINT HA_training_providers_pk PRIMARY KEY,
provider_name VARCHAR2(30) NOT NULL,
qualification_title VARCHAR2(40) NOT NULL,
nfq_Level VARCHAR2(6) DEFAULT 'NA',
comm_date DATE NOT NULL,
award_date DATE NOT NULL
);
CREATE TABLE HA_training_records (
course_id VARCHAR2(16),
staff_id VARCHAR2(8),
CONSTRAINT HA_training_records_pk PRIMARY KEY (course_id, staff_id),
CONSTRAINT HA_tr_rcd_course_id_fk FOREIGN KEY (course_id) REFERENCES HA_training_providers(course_id),
CONSTRAINT HA_tr_rcd_staff_id_fk FOREIGN KEY (staff_id) REFERENCES HA_staff(staff_id)
);
--------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE HA_general_practitioners (
gp_id VARCHAR2(30) CONSTRAINT HA_general_practitioners_pk PRIMARY KEY,
name VARCHAR2(30) NOT NULL,
address VARCHAR2(40) NOT NULL,
telephone VARCHAR2(30) NOT NULL,
mobile_no VARCHAR2(30) NOT NULL,
fax VARCHAR2(30) NOT NULL,
email VARCHAR(100) NOT NULL
);
CREATE TABLE HA_paediatricians (
	pdn_reg_id VARCHAR2(30) CONSTRAINT HA_paediatricians_pk PRIMARY KEY,
	name VARCHAR2(30) NOT NULL,
	secretary VARCHAR2(30),
	department VARCHAR2(30) NOT NULL,
	address VARCHAR2(40) NOT NULL,
	telephone VARCHAR2(20) NOT NULL,
	mobile_no VARCHAR2(20) NOT NULL,
	fax VARCHAR2(20) NOT NULL,
	email VARCHAR2(100) NOT NULL
);
CREATE TABLE HA_public_health_nurses (
	phn_id VARCHAR2(30) CONSTRAINT HA_public_health_nurses_pk PRIMARY KEY,
	name VARCHAR2(30) NOT NULL,
	department VARCHAR2(40) NOT NULL,
	address VARCHAR2(40) NOT NULL,
	telephone VARCHAR2(20) NOT NULL,
	mobile_no VARCHAR2(20) DEFAULT 'none',
	fax VARCHAR2(20) NOT NULL,
	email VARCHAR2(100) NOT NULL
);
--------------------------------------------------------------------------------------------------------------------------
CREATE TABLE HA_patients (
patient_id VARCHAR2(8) CONSTRAINT HA_patients_pk PRIMARY KEY,
first_name VARCHAR2(12) NOT NULL,
last_name VARCHAR2(15) NOT NULL,
dob DATE NOT NULL,
ppsn_no VARCHAR2(8) NOT NULL,
address VARCHAR2(40) NOT NULL,
email VARCHAR2(100),
diagnosis VARCHAR2(20) NOT NULL,
date_diagns DATE NOT NULL,
parentA_name VARCHAR2(30) NOT NULL,
parentA_tel VARCHAR2(15) NOT NULL,
parentB_name VARCHAR2(30),
parentB_tel VARCHAR2(15),
ins_mem_no NUMBER(12,0) DEFAULT 0,
phn_id VARCHAR2(16) NOT NULL,
gp_id VARCHAR2(16) NOT NULL,
hic_id VARCHAR2(16) NOT NULL,
CONSTRAINT HA_patients_ppsn_no_uk UNIQUE (ppsn_no),
CONSTRAINT patients_phn_id_fk FOREIGN KEY (phn_id) REFERENCES HA_public_health_nurses(phn_id),
CONSTRAINT patients_gp_id_fk FOREIGN KEY (gp_id) REFERENCES HA_general_practitioners(gp_id),
CONSTRAINT patients_hic_id_fk FOREIGN KEY (hic_id) REFERENCES HA_health_insurances(comp_id)
);
ALTER TABLE HA_patients
MODIFY ins_mem_no VARCHAR2(20);
ALTER TABLE HA_patients
MODIFY ins_mem_no VARCHAR2(20) DEFAULT 'none';

--along with INNER JOIN TO SPECIFIC SUBTYPE TABLES:--

CREATE TABLE HA_mobility_needs
(
patient_id VARCHAR2(8) CONSTRAINT HA_patient_mobility_needs_pk PRIMARY KEY,
wheelchr_user VARCHAR2(8),
walkg_frame VARCHAR2(8),
crutchs VARCHAR2(8),
CONSTRAINT mob_needs_patient_id_fk FOREIGN KEY (patient_id) REFERENCES HA_patients(patient_id)
);
select * from HA_patients p inner join HA_mobility_needs mn on p.patient_id = mn.patient_id;-- uses aliases for table names to keep concise--
--or----or--
select * from HA_patients p left join HA_mobility_needs mn on p.patient_id = mn.patient_id;

CREATE TABLE HA_feeding_needs
(
patient_id VARCHAR2(8) CONSTRAINT HA_patient_feeding_needs_pk PRIMARY KEY,
peg_fed VARCHAR2(8),
spoon_fed VARCHAR2(8),
food_allergy VARCHAR2(8),
intolerance VARCHAR2(8),
CONSTRAINT feed_needs_patient_id_fk FOREIGN KEY (patient_id) REFERENCES HA_patients(patient_id)
);
select * from HA_patients p inner join HA_feeding_needs fn on p.patient_id = fn.patient_id;

CREATE TABLE HA_behavioural_needs
(
patient_id VARCHAR2(8) CONSTRAINT HA_behavioural_needs_pk PRIMARY KEY,
mild_behavioural VARCHAR2(20),
moderate_behavioural VARCHAR2(20),
severe_behavioural VARCHAR2(20),
CONSTRAINT behav_needs_patient_id_fk FOREIGN KEY (patient_id) REFERENCES HA_patients(patient_id)
);
select * from HA_patients p inner join HA_behavioural_needs bn on p.patient_id = bn.patient_id;

CREATE TABLE HA_complex_needs
(
patient_id VARCHAR2(8) CONSTRAINT HA_patient_complex_needs_pk PRIMARY KEY,
communication VARCHAR2(8),
toileting VARCHAR2(8),
blindness VARCHAR2(8),
hearing VARCHAR2(8),
combination VARCHAR2(8),
other VARCHAR2(8),
CONSTRAINT complex_needs_patient_id_fk FOREIGN KEY (patient_id) REFERENCES HA_patients(patient_id)
);
select * from HA_patients p inner join HA_complex_needs cn on p.patient_id = cn.patient_id;
------------------------------------------------------------------------------------------------------------------
CREATE TABLE HA_prescriptions (
date_psn DATE NOT NULL,
patient_id VARCHAR2(8) NOT NULL,
psn_type VARCHAR2(9) NOT NULL,
gp_reg_id VARCHAR2(30),
pdn_reg_id VARCHAR2(30),
CONSTRAINT HA_prescriptions_pk PRIMARY KEY (patient_id, gp_reg_id, pdn_reg_id),
CONSTRAINT presc_patient_id_fk FOREIGN KEY (patient_id) REFERENCES HA_patients(patient_id),
CONSTRAINT prescs_gp_reg_id_fk FOREIGN KEY (gp_reg_id) REFERENCES HA_general_practitioners(gp_id),
CONSTRAINT presc_pdn_reg_id_fk FOREIGN KEY (pdn_reg_id) REFERENCES HA_paediatricians(pdn_reg_id)
);

ALTER TABLE HA_prescriptions DROP CONSTRAINT HA_prescriptions_pk;
ALTER TABLE HA_prescriptions
ADD CONSTRAINT HA_prescriptions_pk PRIMARY KEY (date_psn, patient_id, gp_reg_id, pdn_reg_id);

ALTER TABLE HA_prescriptions DROP CONSTRAINT HA_prescriptions_pk;
ALTER TABLE HA_prescriptions
ADD CONSTRAINT HA_prescriptions_pk PRIMARY KEY (date_psn, patient_id);
--------------------------------------------------------------------------------------------------------------------
CREATE TABLE HA_paediatric_reports (
rpt_date DATE NOT NULL,
pdn_id VARCHAR2(30), 
patient_id VARCHAR2(30),
CONSTRAINT HA_paed_rpt_pk PRIMARY KEY (pdn_id, patient_id),
CONSTRAINT HA_paed_pdn_fk FOREIGN KEY (pdn_id) REFERENCES HA_paediatricians(pdn_reg_id),
CONSTRAINT HA_paed_patient_fk FOREIGN KEY (patient_id) REFERENCES HA_patients(patient_id)
);
--------------------------------------------------------------------------------------------------------------------
ALTER TABLE HA_paediatric_reports DROP CONSTRAINT HA_paed_rpt_pk;
ALTER TABLE HA_paediatric_reports
ADD CONSTRAINT HA_paed_rpts_pk PRIMARY KEY (rpt_date, pdn_id, patient_id);

CREATE TABLE HA_referrals (
ref_no VARCHAR2(10) CONSTRAINT HA_referrals_pk PRIMARY kEY,
referral_date DATE NOT NULL,
phn_id VARCHAR2(30),
pdn_reg_id VARCHAR2(30),
CONSTRAINT phn_fk FOREIGN KEY (phn_id) REFERENCES HA_public_health_nurses(phn_id) ON DELETE CASCADE,
CONSTRAINT pdn_fk FOREIGN KEY (pdn_reg_id) REFERENCES HA_paediatricians(pdn_reg_id) ON DELETE CASCADE
);

CREATE TABLE HA_stay_approval (
approval_ref VARCHAR2(30) CONSTRAINT HA_stay_approval_pk PRIMARY KEY,
approval_date DATE  NOT NULL,
phn_id VARCHAR2(30) NOT NULL,
CONSTRAINT HA_appr_fk FOREIGN KEY (phn_id) REFERENCES HA_public_health_nurses(phn_id)
);
---------------------------------------------------------------------------------------------------------------------------
-- ONE TABLE SUBTYPE IMPLEMENTATION --
CREATE TABLE HA_stay_records (
patient_id VARCHAR2(8) CONSTRAINT stay_pat_fk REFERENCES HA_patients(patient_id),
approval_ref VARCHAR2(30) CONSTRAINT stay_appr_fk REFERENCES HA_stay_approval(approval_ref),
dept_id VARCHAR2(30) CONSTRAINT stay_dpt_fk REFERENCES HA_departments(dpt_id),
comm_date DATE NOT NULL,
duration_days NUMBER(3, 0) NOT NULL,
duration_nights NUMBER (3, 0) NOT NULL,
assigned_carer VARCHAR2(30) CONSTRAINT assig_car_fk REFERENCES HA_staff(staff_id) ON DELETE SET NULL, 
relief_carer VARCHAR2(30) CONSTRAINT rel_car_fk REFERENCES HA_staff(staff_id) ON DELETE SET NULL,
med_consent_from VARCHAR2(30),
day1_act VARCHAR2(40),
day2_act VARCHAR(40),
day3_act VARCHAR(40),
cinema_name VARCHAR(30),
garden_name VARCHAR(30),
pool_name VARCHAR(30),
CONSTRAINT HA_stay_records_pk PRIMARY KEY (patient_id, approval_ref, dept_id),
CONSTRAINT day1_cin_chk CHECK (day1_act = 'CIN' AND cinema_name is not null AND garden_name is null AND pool_name is null),
CONSTRAINT day1_garD_chk CHECK (day1_act = 'GDN' AND garden_name is not null AND cinema_name is null AND pool_name is null),
CONSTRAINT day1_pl_chk CHECK (day1_act = 'PL' AND pool_name is not null AND cinema_name is null AND garden_name is null),
CONSTRAINT day2_cin_chk CHECK (day2_act = 'CIN' AND cinema_name is not null AND garden_name is null AND pool_name is null),
CONSTRAINT day2_gard_chk CHECK (day2_act = 'GDN' AND garden_name is not null AND cinema_name is null AND pool_name is null),
CONSTRAINT day2_pl_chk CHECK (day2_act = 'PL' AND pool_name is not null AND cinema_name is null AND garden_name is null),
CONSTRAINT day3_cin_chk CHECK (day3_act = 'CIN' AND cinema_name is not null AND garden_name is null AND pool_name is null),
CONSTRAINT day3_gard_chk CHECK (day3_act = 'GDN' AND garden_name is not null AND cinema_name is null AND pool_name is null),
CONSTRAINT day3_pl_chk CHECK (day3_act = 'PL' AND pool_name is not null AND cinema_name is null AND garden_name is null)
);

-- PROBLEM WITH CONFLICTING CHECK CONSTRAINT,  REVISED SCRIPT:
--Ideally shoul have separate day activity record....
DROP TABLE HA_stay_records;

CREATE TABLE HA_stay_records (
patient_id VARCHAR2(8) CONSTRAINT stay_pat_fk REFERENCES HA_patients(patient_id),
approval_ref VARCHAR2(30) CONSTRAINT stay_appr_fk REFERENCES HA_stay_approval(approval_ref),
dept_id VARCHAR2(30) CONSTRAINT stay_dpt_fk REFERENCES HA_departments(dpt_id),
comm_date DATE NOT NULL,
duration_days NUMBER(3, 0) NOT NULL,
duration_nights NUMBER (3, 0) NOT NULL,
assigned_carer VARCHAR2(30) CONSTRAINT assig_car_fk REFERENCES HA_staff(staff_id) ON DELETE SET NULL, 
relief_carer VARCHAR2(30) CONSTRAINT rel_car_fk REFERENCES HA_staff(staff_id) ON DELETE SET NULL,
med_consent_from VARCHAR2(30),
day_act VARCHAR2(40), - - --activity type diffrentiator column
cinema_name VARCHAR(30),
garden_name VARCHAR(30),
pool_name VARCHAR(30),
CONSTRAINT HA_stay_records_pk PRIMARY KEY (patient_id, approval_ref, dept_id),
CONSTRAINT day_chk CHECK (day_act = 'CIN' AND cinema_name is not null AND garden_name is null AND pool_name is null
OR day_act = 'GDN' AND garden_name is not null AND cinema_name is null AND pool_name is null
OR day_act = 'PL' AND pool_name is not null AND cinema_name is null AND garden_name is null)
); -- one long bracket statement the same as 3 no separate ones
--------------------------------------------------------------------------------------------------------------------------
CREATE TABLE HA_therapists (
ther_id VARCHAR2(30) CONSTRAINT HA_therapists_pk PRIMARY KEY,
name VARCHAR2(30) NOT NULL,
dpt_name VARCHAR2(30) NOT NULL,
dpt_address VARCHAR2(50) NOT NULL,
ther_grade VARCHAR2(10) NOT NULL,
telephone VARCHAR2(16) NOT NULL,
mob_no VARCHAR2(16) DEFAULT 'na',
fax VARCHAR2(16) DEFAULT 'na',
email VARCHAR2(25) DEFAULT 'na'
);

--TWO TABLE SUBTYPE IMPLEMENTATION---

DROP TABLE HA_speech_and_language_report;

CREATE TABLE HA_speech_and_language_reports (
rpt_date DATE NOT NULL,
ther_id VARCHAR2(30) NOT NULL,
patient_id VARCHAR2(8) NOT NULL,
core_language NUMBER(15) NOT NULL,
receptive_language NUMBER(15) NOT NULL,
expressive_language NUMBER(15) NOT NULL,
language_content NUMBER(15) NOT NULL,
CONSTRAINT HA_ther_id_fk FOREIGN KEY (ther_id) REFERENCES HA_therapists(ther_id) ON DELETE SET NULL,
CONSTRAINT HA_sl_pat_id_fk FOREIGN KEY (patient_id) REFERENCES HA_patients(patient_id) ON DELETE SET NULL,
CONSTRAINT HA_sl_rpt_pk PRIMARY KEY (rpt_date, ther_id, patient_id)
);

DROP TABLE HA_occupational_therapy_report;

CREATE TABLE HA_occuptl_therapy_reports (
rpt_date DATE NOT NULL,
ther_id VARCHAR2(30) CONSTRAINT HA_ot_rpt_ther_fk REFERENCES HA_therapists(ther_id) ON DELETE SET NULL,
patient_id VARCHAR2(8) CONSTRAINT HA_ot_rpt_pat_fk REFERENCES HA_patients(patient_id) ON DELETE SET NULL,
ot_equip_splr VARCHAR2(30) DEFAULT 'NA',
gross_motor_skills VARCHAR2(25),
fine_motor_skills VARCHAR2(25),
oral_motor_skills VARCHAR2(25),
spatial_perception VARCHAR2(25),
toileting_needs VARCHAR2(25),
CONSTRAINT HA_ot_rpt_pk PRIMARY KEY (rpt_date, ther_id, patient_id)
);

DROP TABLE HA_psychology_report;

CREATE TABLE HA_psychology_reports (
rpt_date DATE NOT NULL,
ther_id VARCHAR2(30) CONSTRAINT HA_ps_rpt_ther_fk REFERENCES HA_therapists(ther_id) ON DELETE SET NULL,
patient_id VARCHAR2(8) CONSTRAINT HA_ps_rpt_pat_fk REFERENCES HA_patients(patient_id) ON DELETE SET NULL,
very_superior VARCHAR2(25),
superior VARCHAR2(25),
high_average VARCHAR2(25),
average VARCHAR2(25),
low_average VARCHAR2(25),
borderline VARCHAR2(25),
mild VARCHAR2(25),
moderate VARCHAR2(25),
severe VARCHAR2(25),
profound VARCHAR2(25),
CONSTRAINT HA_ps_rpt_pk PRIMARY KEY (rpt_date, ther_id, patient_id)
);

DROP TABLE HA_physiotherapy_report;

CREATE TABLE HA_physiotherapy_reports (
rpt_date DATE NOT NULL,
ther_id VARCHAR2(30) CONSTRAINT HA_phys_rpt_ther_fk REFERENCES HA_therapists(ther_id) ON DELETE SET NULL,
patient_id VARCHAR2(8) CONSTRAINT HA_phys_rpt_pat_fk REFERENCES HA_patients(patient_id) ON DELETE SET NULL,
physio_equip_splr VARCHAR2(30) DEFAULT 'NA',
low_muscle_tone VARCHAR2(25),
lymed_suit VARCHAR2(25),
gross_motor VARCHAR2(25),
fine_motor VARCHAR2(25),
trike VARCHAR2(25),
balance_bike VARCHAR2(25),
support_chair VARCHAR2(25),
walker VARCHAR2(25),
wheelchair_usr VARCHAR2(25),
CONSTRAINT HA_phys_rpt_pk PRIMARY KEY (rpt_date, ther_id, patient_id)
);
-------------------------------------------------------------------------------------------------------------------